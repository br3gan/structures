#include <iostream>

struct Node{
    Node* left;
    Node* right;
    int data;
};

class Tree{

public:
    
    Tree(){
        root = NULL;            
    }

    ~Tree(){
        delete_tree();    
    }

    void insert_node(int key);
    Node* search_node(int key);
    void delete_tree();

private:
    
    void insert_node(int key, Node* leaf);
    Node* search_node(int key, Node* leaf); 
    void delete_node(Node* subtree);
    
    void create_node(int key, Node** newnode);

    Node* root;
};
