#include "tree.h"

void Tree::create_node(int key, Node** newnode){
    *newnode=new Node;
    (*newnode)->data = key;
    (*newnode)->left = NULL;
    (*newnode)->right= NULL;
}

void Tree::insert_node(int key){
    if (root==NULL){
        create_node(key, &root);
    }
    else{
        insert_node(key, root);

    }    
    return;
}

Node* Tree::search_node(int key){
    
    if (root==NULL){
        return NULL;    
    }    

    return search_node(key, root);
}

void Tree::delete_tree(){
    delete_node(root);
}

void Tree::insert_node(int key, Node* leaf){
    if (leaf->data > key){
        if(leaf->left!=NULL){
            insert_node(key, leaf->left);
            return;
        }
        else{
            create_node(key, &leaf->left);
            return;
        }
    }
    if (leaf->data <= key){
        if(leaf->right!=NULL){
            insert_node(key, leaf->right);
            return;
        }
        else{
            create_node(key, &leaf->right);
            return;
        }
    }
}

Node* Tree::search_node(int key, Node* leaf){
    if(leaf==NULL){
        return NULL;
    }
    if(leaf->data==key){
        return leaf;
    }
    if(leaf->data > key){
        return search_node(key, leaf->left);
    }
    if(leaf->data < key){
        return search_node(key, leaf->right);
    }

    // like it is ever gonna happen
    return NULL;
}

void Tree::delete_node(Node* subroot){
    if(subroot==NULL){
        return;
    }    
    delete_node(subroot->left);
    delete_node(subroot->right);
    delete subroot;
}

int main(int argc, char* argv[]){
 
    Tree tree;
    tree.insert_node(10);
    tree.insert_node(11);
    tree.insert_node(12);
    tree.insert_node(13);
    tree.insert_node(8);
    tree.insert_node(9);
    tree.insert_node(7);
    tree.insert_node(0);

    Node* node = tree.search_node(10);

    if (node == NULL){
        std::cout << "Not found" << std::endl;
    }
    else{
        std::cout << "Key found" << std::endl;
    }

    node = tree.search_node(11);

    if (node == NULL){
        std::cout << "Not found" << std::endl;
    }
    else{
        std::cout << "Key found" << std::endl;
    }

    node = tree.search_node(12);

    if (node == NULL){
        std::cout << "Not found" << std::endl;
    }
    else{
        std::cout << "Key found" << std::endl;
    }

    node = tree.search_node(13);

    if (node == NULL){
        std::cout << "Not found" << std::endl;
    }
    else{
        std::cout << "Key found" << std::endl;
    }

    node = tree.search_node(6);

    if (node == NULL){
        std::cout << "Not found" << std::endl;
    }
    else{
        std::cout << "Key found" << std::endl;
    }

    node = tree.search_node(13);

    if (node == NULL){
        std::cout << "Not found" << std::endl;
    }
    else{
        std::cout << "Key found" << std::endl;
    }

    node = tree.search_node(6);

    if (node == NULL){
        std::cout << "Not found" << std::endl;
    }
    else{
        std::cout << "Key found" << std::endl;
    }
} 
