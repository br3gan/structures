#include "list.h"

Node& List::operator[](int position){
    
    if (position>=m_size){
        return *m_head;
    }

    Node* tmp=m_head;
    for(int i=0 ; i<m_size; i++){
        if (i == position)
            break;
        tmp=tmp->next;
    } 
    return *tmp;
}

const Node& List::operator[](int position) const{
    
    if (position>=m_size){
        return *m_head;
    }

    Node* tmp=m_head;
    for(int i=0 ; i<m_size; i++){
        if (i == position)
            break;  
        tmp=tmp->next;
    }
    return *tmp;
}

void List::push_back(int value){

    if (m_head == NULL){
        m_head = new Node;
        m_head->value = value;
        m_head->next = NULL;
    }
    else{

        Node* new_node = new Node;
        new_node->value=value;
        new_node->next=NULL;

        Node* tmp=m_head;
        while(tmp->next!=NULL){
            tmp = tmp->next;
        };
        tmp->next = new_node;       
    }
    m_size++;
}

void List::pop_back(){
    
    if(m_head==NULL)
        return;
    
    Node* tmp = m_head;
    Node* prev = m_head;
    while (tmp -> next!=NULL){
         prev=tmp;
         tmp=tmp->next;
    }
    delete tmp;
    prev->next=NULL;    
    m_size--;
}

void List::push_front(int value){
    
    Node* new_head=new Node;
    new_head->value=value;
    new_head->next=m_head;
    m_head=new_head;
    m_size++;
}

void List::pop_front(){
    
    if(m_head==NULL)
        return;

    Node* old_head=m_head;
    m_head=m_head->next;
    delete old_head;
    m_size--;
}

int List::find(int value) {
    
    Node* tmp = m_head;
    for(int i=0; i<m_size; i++){
        if (tmp->value == value){
            return i;
        }
        tmp=tmp->next;
    }        
    return -1;
}

const int List::find(int value) const{
    
    Node* tmp = m_head;
    for(int i=0; i<m_size; i++){
        if (tmp->value == value){
            return i;
        }
        tmp=tmp->next;
    }        
    return -1;
}

const int List::size() const{
    return m_size;
}

const Node& List::first() const{
    if(m_size)
        return (*this)[0];
}

const Node& List::last() const{
    if (m_size)
        return (*this)[m_size-1];
}

void List::delete_node(int value){

    Node* tmp = m_head;
    Node* prev = m_head;
    
    while(tmp!=NULL && tmp->value!=value){
        prev = tmp;
        tmp = tmp->next;
    }

    if(tmp!=NULL){
        prev->next=tmp->next;
        delete tmp;
        m_size--;
    }
}

void List::delete_list(){
    int initial_size = m_size;
    for(int i=0; i< initial_size; i++){
        this->pop_front();
    }
}

void List::print() const{
    for(int i = 0 ; i<m_size ; i++){
        std::cout << "Item in position " << i << " has value " << (*this)[i].value << std::endl;    
    }
    std::cout << std::endl;
}

int main(int argc, char* argv[]){
    List list;
    
    list.push_back(5);
    list.push_front(10);
    list.push_back(11); 
    list.push_front(12); 
    list.push_back(13); 
    list.push_front(14); 
    list.push_back(0); 
    list.push_front(1); 
    list.push_back(2);
    
    list.print();

    list.pop_back();
    list.pop_front();

    list.print();

    int position;
    position = list.find(5);
    if(position>=0) 
        std::cout << "Value 5 Found at position " << position << std::endl;

    position = list.find(10);
    if(position>=0)
        std::cout << "Value 10 Found at position " << position << std::endl;
        

    position = list.find(15);
    if(position>=0)
        std::cout << "Value 15 Found at position " << position << std::endl;
    
    position = list.find(2);
    if(position>=0)
        std::cout << "Value 2 Found at position " << position << std::endl;
    

 
}











