#include <iostream>

struct Node{
    int value;
    Node* next;
};  

class List{

private:

    Node* m_head;
    int m_size;

    void delete_list();

public:
        
    List(){
        m_head=NULL;
        m_size=0;
    }

    ~List(){
        delete_list();
    }

    Node& operator[](int position);
    const Node& operator[](int position) const; 
 
    void push_back(int value);
    void pop_back();
    void push_front(int value);
    void pop_front();
   
    const int find(int value) const; 
    int find(int value);
    
    const int size() const;

    const Node& first() const;
    const Node& last() const;
    
    void delete_node(int value);
    void print() const;
};

